# بِسْمِ ٱللّٰهِ ٱلرَّحْمٰنِ ٱلرَّحِيمِ

### TROUBLESHOOT
#### I: Server is not accessible from `localhost` when running using docker.
**S**: You should set `hostname` to `0.0.0.0` in `dot env` file as following:
```dotenv
HOST_IP="0.0.0.0"
```