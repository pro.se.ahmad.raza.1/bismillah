FROM node:alpine

WORKDIR /app

EXPOSE 4001
CMD [ "npm", "run", "dev" ]