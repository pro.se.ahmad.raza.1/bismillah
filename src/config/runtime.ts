export const opts = {
  host: process.env.HOST_IP || "127.0.0.1",
  port: parseInt(process.env.PORT || "4001") | 4001,
};
