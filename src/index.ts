import { config } from "dotenv";
config();

import { createServer } from "http";
import path from "path";
import { opts } from "./config/runtime";

import express from "express";
import morgan from "morgan";
import serveStatic from "serve-static";
import IndexRouter from "./routes";

const app = express();
const server = createServer(app);

app.use(morgan("dev"));
app.use(serveStatic(path.join(__dirname, "..", "public")));
app.use("/api/v1", IndexRouter);

server.listen(opts.port, opts.host);
