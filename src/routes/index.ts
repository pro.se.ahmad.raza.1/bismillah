import { Router } from "express";
const IndexRouter = Router();
import * as app from "../config/app.json";

IndexRouter.get("/about", (_, res) => {
  res.json({
    version: app.version,
    authors: app.authors,
  });
});

IndexRouter.get("/version", (_, res) => {
  res.json({
    version: app.version,
    release: app.release,
  });
});

export default IndexRouter;